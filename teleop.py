import pika
import sys
import json
credentials = pika.PlainCredentials('adri1177', 'test')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='adri111777.myftp.org',
                                                                port=5672,
                                                                virtual_host="/",
                                                                credentials=credentials))




if sys.platform == 'win32':

    import msvcrt

    def keypressed():
        key = msvcrt.getch().decode("utf-8")
        if key == "w":
            return "Escape"
        else:
            return key.decode("ANSI")

elif sys.platform == 'linux':

    import os
    import termios
    TERMIOS = termios

    def keypressed():
        fd = sys.stdin.fileno()
        old = termios.tcgetattr(fd)
        new = termios.tcgetattr(fd)
        new[3] = new[3] & ~TERMIOS.ICANON & ~TERMIOS.ECHO
        new[6][TERMIOS.VMIN] = 1
        new[6][TERMIOS.VTIME] = 0
        termios.tcsetattr(fd, TERMIOS.TCSANOW, new)
        key = None
        try:
            key = os.read(fd, 4)
            if key == b'\x1b':
                key = "Escape"
            else:
                key = key.decode()
        finally:
            termios.tcsetattr(fd, TERMIOS.TCSAFLUSH, old)
        return key

while(1):
    key = keypressed().lower()
    print(key)
    if key == "w":
        json_msg = {"topic": "/mobile_base/commands/velocity",
                    "msg": {
                        "linear":
                            {"y": 0.0,
                             "x": 0.2,
                             "z": 0.0
                             },
                        "angular": {
                            "y": 0.0,
                            "x": 0.0,
                            "z": 0.0}
                    },
                    "op": "publish"}
    if key == "s":
        json_msg = {"topic": "/mobile_base/commands/velocity",
                    "msg": {
                        "linear":
                            {"y": 0.0,
                             "x": -0.2,
                             "z": 0.0
                             },
                        "angular": {
                            "y": 0.0,
                            "x": 0.0,
                            "z": 0.0}
                    },
                    "op": "publish"}
    if key == "a":
        json_msg = {"topic": "/mobile_base/commands/velocity",
                    "msg": {
                        "linear":
                            {"y": 0.0,
                             "x": 0.0,
                             "z": 0.0
                             },
                        "angular": {
                            "y": 0.0,
                            "x": 0.0,
                            "z": 0.2}
                    },
                    "op": "publish"}
    if key == "d":
        json_msg = {"topic": "/mobile_base/commands/velocity",
                    "msg": {
                        "linear":
                            {"y": 0.0,
                             "x": 0.0,
                             "z": 0.0
                             },
                        "angular": {
                            "y": 0.0,
                            "x": 0.0,
                            "z": -0.2}
                    },
                    "op": "publish"}
    channel = connection.channel()
    channel.publish(exchange='',
                    routing_key="id",
                    body=json.dumps(json_msg),
                    properties=pika.BasicProperties(
                        delivery_mode=1,  # make message persistent
                    ))
