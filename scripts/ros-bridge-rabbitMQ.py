#!/usr/bin/env python

# ----------------------------------------------------------------------------------------
# authors, description, version
# ----------------------------------------------------------------------------------------
# Endre Eres
# ROSBridge-Kafka multiclient: kafka_ros_bridge.py
# V.0.1.0.
# ----------------------------------------------------------------------------------------

import time
import json
import threading
import socket
import pika
from std_msgs.msg import String
#from kafka import KafkaProducer
#from kafka import KafkaConsumer



class Bridge():

    def __init__(self, sock=None):

        if sock == None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

        self.spin()

    def queue_to_ros(self):
        def queue_to_ros_callback():
            # consumer = KafkaConsumer('to_ros',
            #                          bootstrap_servers='localhost:9092',
            #                          value_deserializer=lambda m: json.loads(m.decode('ascii')),
            #                          auto_offset_reset='latest',
            #                          consumer_timeout_ms=1000)

             HOST = "localhost"
             PORT = 9090
             s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
             s.connect((HOST, PORT))

             while (1):
                    time.sleep(1)
                    print("b")
                #print("consumer to ros callback")
                # for message in consumer:
                #     if message.value['op'] != "subscribe":
                #         # print (message.value)
                #         s.send(json.dumps(message.value))
                #     else:
                #         pass

        t = threading.Thread(target=queue_to_ros_callback)
        t.daemon = True
        t.start()

    def producer_from_ros(self):
        def producer_from_ros_callback():
            # consumer = KafkaConsumer('to_ros',
            #                          bootstrap_servers='localhost:9092',
            #                          value_deserializer=lambda m: json.loads(m.decode('ascii')),
            #                          auto_offset_reset='latest',
            #                          consumer_timeout_ms=1000)
            while (1):
                 time.sleep(1)
                #print("producer from ros callback")
                # for message in consumer:
                #     if message.value['op'] == "subscribe" or message.value['op'] == "unsubscribe":
                 print("a")
                 self.connect('localhost', 9090)
                 time.sleep(0.3)
                 self.send(json.dumps({"op": "subscribe", "topic": "/chatter2", "type": "std_msgs/String"}))
                 time.sleep(0.3)
                 self.receive()

        t = threading.Thread(target=producer_from_ros_callback)
        t.daemon = True
        t.start()

    def connect(self, host, port):
        def connect_callback():
            TCP_IP = 'localhost'
            TCP_PORT = 9090
            try:
                self.sock.connect((TCP_IP, TCP_PORT))
            except:
                print("Transport endpoint is already connected")
        t = threading.Thread(target=connect_callback)
        t.daemon = True
        t.start()

    def send1234(self, msg):
        def send_callback():
            self.sock.send(json.dumps(msg))

        t = threading.Thread(target=send_callback)
        t.daemon = True
        t.start()

    def receive(self):
        def receive_callback():
            # producer = KafkaProducer(bootstrap_servers='localhost:9092',
            #                          value_serializer=lambda m: json.dumps(m).encode('ascii'))
            while (1):
                print("RECV")
                data = self.sock.recv(4096)
                print("DATA")
                print(data)
                if not data: break
                #producer.send('from_ros', data)
                print(data)
            self.sock.close()

        t = threading.Thread(target=receive_callback)
        t.daemon = True
        t.start()

    def spin(self):
        self.queue_to_ros()
        self.producer_from_ros()
        # while (1):
        #     pass


if __name__ == '__main__':
    try:
        Bridge()
    except KeyboardInterrupt:
        pass