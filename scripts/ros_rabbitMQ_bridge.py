import json
import pika
import roslibpy
import websocket
#from __future__ import print_function

FORBIDDEN_TOPICS= ['/rosout','/rosout_agg']

#
# def send_info(message):
#     print(message)
#
# def get_topics(topics):
#     print(topics)
#     for idx,data in enumerate(topics.data["types"]):
#         if topics.data["topics"][idx] not in FORBIDDEN_TOPICS:
#             topic_object = roslibpy.Topic(ros,topics.data["topics"][idx],topics.data["types"][idx])
#             topic_object.advertise()
#             topic_object.subscribe(send_info)
#
# ws = websocket.create_connection("ws://localhost:9090",timeout=10)
# while(True):
#     ws.send(json.dumps({'op': 'call_service', 'service': '/rosapi/topics', 'arg': 'rosapi/Topics'}))
#

credentials = pika.PlainCredentials('test', 'test')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.0.104',
                                                               port=5672,
                                                               virtual_host='/',
                                                               credentials=credentials))

def callback(ch, method, properties, body):
    print(" [x] Procesing %r" % body)

    print(" [x] Processed")

while True:

    channel = connection.channel()
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback, queue='from_ros')

    # try:
    channel.start_consuming()

    # except:
    #     connection = pika.BlockingConnection(pika.ConnectionParameters(host=configuration.HOST,
    #                                                                          port=configuration.PORT,
    #                                                                          virtual_host=configuration.virtual_host,
    #                                                                          credentials=credentials))