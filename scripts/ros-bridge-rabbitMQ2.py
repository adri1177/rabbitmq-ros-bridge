import time
import json
import threading
import socket
import pika


class Bridge():
    SERVER = "127.0.0.1"
    PORT = 9090
    MSGLEN = 4096
    ALLOWED_TOPICS = ['/scan', '/tf','/tf_static','','',
                        '','','','']

    credentials = pika.PlainCredentials('adri1177', 'test')

    def __init__(self, sock=None, connection=None,read_connection=None):

        if sock == None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock
        if connection == None:
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='172.16.224.129',
                                                                                port=5672,
                                                                                virtual_host="/",
                                                                                credentials=self.credentials))
        else:
            self.connection = connection
        if read_connection == None:
            self.read_connection = pika.BlockingConnection(pika.ConnectionParameters(host='172.16.224.129',
                                                                                port=5672,
                                                                                virtual_host="/",
                                                                                credentials=self.credentials))
        else:
            self.read_connection = read_connection
        self.spin()

    def queue_to_ros(self):
        def queu_to_ros_callback():
            def process_msg(ch, method, properties, body):
                #print(" [x] Procesing %r" % body)
                self.sendmessage(body)
                ch.basic_ack(delivery_tag=method.delivery_tag)
                print(" [x] Processed")

            while True:
                channel = self.read_connection.channel()
                channel.queue_declare("id", durable=True)
                channel.basic_qos(prefetch_count=1)
                channel.basic_consume(process_msg, queue="rosin")
                channel.start_consuming()

        t = threading.Thread(target=queu_to_ros_callback)
        t.daemon = True
        t.start()

    def ros_to_rabbit(self):
        def ros_to_rabbit_callback():
            already_subscribed=[]
            while (1):
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((self.SERVER, 9090))
                s.send((json.dumps({'op': 'call_service', 'service': '/rosapi/topics', 'arg': 'rosapi/Topics'})).encode('utf-8'))
                topics_response = s.recv(4096)
                topics = json.loads(topics_response.decode('utf-8'))
                print(topics)
                try:
                    for idx, data in enumerate(topics["values"]["topics"]):
                        if topics["values"]["topics"][idx]  in self.ALLOWED_TOPICS:
                            if topics["values"]["topics"][idx] not in already_subscribed:
                                self.sendmessage(json.dumps({"op": "subscribe", "topic": topics["values"]["topics"][idx],
                                                         "type": topics["values"]["types"][idx]}))
                                already_subscribed.append(topics["values"]["topics"][idx])
                                #time.sleep(1)
                except:
                    print("No communication with master")
                time.sleep(10)

        t = threading.Thread(target=ros_to_rabbit_callback)
        t.daemon = True
        t.start()

    def connectSocket(self):
        def connect_callback():
            try:
                self.sock.connect((self.SERVER, self.PORT))
            except Exception as e:
                print(e)

        t = threading.Thread(target=connect_callback)
        t.daemon = True
        t.start()

    def sendmessage(self, msg):
        def send_callback():
            #print("Sending"+msg)
            self.connectSocket()
            try:
                self.sock.send(msg.encode('utf-8'))
            except Exception as e:
                print(e)
                print("HOLA")

        t = threading.Thread(target=send_callback)
        t.daemon = True
        t.start()

    def send_message_to_rabbitMQ(self,msg):
        #print(msg)
        json_msg ={"id":"id","data":json.loads(msg)}
        channel = self.connection.channel()
        if(json_msg["data"]["topic"] == "/scan"):
            #print("#######################")
            msg = msg.replace("null","0")
            json_msg ={"id":"id","data":json.loads(msg)}
        channel.queue_declare(json_msg["data"]["topic"], durable=False)
        channel.publish(exchange='',
                        routing_key="rosout",#json_msg["data"]["topic"],
                        body=json.dumps(json_msg),
                        properties=pika.BasicProperties(
                            delivery_mode=1,  # make message persistent
                        ))
        channel.close()

    def receive(self):
        def receive_callback():
            while (1):
                message = ''
                counter = 0
                while True:
                    chars = self.sock.recv(4096)
                    for char in chars:
                        message += char
                        if char == "{":
                            counter = counter + 1
                        elif char == "}":
                            counter = counter - 1
                            if counter == 0:
                                self.send_message_to_rabbitMQ(message)
                                message = ""
        t = threading.Thread(target=receive_callback)
        t.daemon = True
        t.start()

    def spin(self):
        self.queue_to_ros()
        self.ros_to_rabbit()
        self.connectSocket()
        self.receive()
        while (1):
            pass


if __name__ == '__main__':
    try:
        Bridge()
    except KeyboardInterrupt:
        pass
