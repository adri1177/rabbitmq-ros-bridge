#!/usr/bin/python

# from websocket import create_connection
# import json
#
# ws = create_connection("ws://localhost:9090",timeout=10)
# print "Sending 'Hello, World'..."
# ws.send(json.dumps({'op': 'call_service', 'service': '/rosapi/topics', 'arg': 'rosapi/Topics'}))
# ws.send(json.dumps({"op": "subscribe", "topic": "/chatter", "type": "std_msgs/String"}))
# print "Sent"
# print "Receiving..."
# while(1):
#     result =  ws.recv()
#
#     asdf = json.loads(result)
#     print "Received '%s'" % result
# ws.close()

import socket
import json

MSGLEN = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 9090))
s.send(json.dumps({"op": "subscribe", "topic": "/chatter", "type": "std_msgs/String"}))
chunks = []
bytes_recd = 0
while bytes_recd < MSGLEN:
    chunk = s.recv(min(MSGLEN - bytes_recd, 2048))
    if chunk == '':
        raise RuntimeError("socket connection broken")
    chunks.append(chunk)
    print chunk
    bytes_recd = bytes_recd + len(chunk)
print ''.join(chunks)
